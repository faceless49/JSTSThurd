import axios from 'axios';
const key = '8cf97189';

const configOMB = {
    baseURL: 'http://www.omdbapi.com/',
    withCredentials: true,
    headers: {
        "API-KEY": key,
    },
};
const axiosInstance = axios.create(configOMB);

const API = {
    searchFilmsByTitle: (title: string) => {
        return axiosInstance.post(`?s=${title}`)
    },
    searchFilmsByType: (title: string, type: string) => {
        return axiosInstance.post(`?t=${title}&type=${type}`)
    }
};


export default API;
