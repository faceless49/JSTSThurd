console.log('lesson 2');

// Lexical environment
//

//// Closure
// https://learn.javascript.ru/closure
// https://developer.mozilla.org/ru/docs/Web/JavaScript/Closures
// https://medium.com/@stasonmars/%D0%BF%D0%BE%D0%BD%D0%B8%D0%BC%D0%B0%D0%B5%D0%BC-%D0%B7%D0%B0%D0%BC%D1%8B%D0%BA%D0%B0%D0%BD%D0%B8%D1%8F-%D0%B2-javascript-%D1%80%D0%B0%D0%B7-%D0%B8-%D0%BD%D0%B0%D0%B2%D1%81%D0%B5%D0%B3%D0%B4%D0%B0-c211805b6898
// https://www.youtube.com/watch?v=pahO5XjnfLA

//// Сurrying
// https://learn.javascript.ru/currying-partials
// https://stasonmars.ru/javascript/ponimaem-carrirovanie-v-javascript/

// Pattern Module
// https://habr.com/ru/company/ruvds/blog/419997/

// Recursion
// https://learn.javascript.ru/recursion
// https://www.youtube.com/watch?v=Kuq6oIN3PH0


// * Допущения скопа
// 1 проход JS

// let globalScope = {
//   outerScope: null, // default значение у глобала
//   // f: 'Function', // 1 проход
//   // c: undefined,  // 1 проход // 2 проход появляется 50
//   // a: 10, // 2 проход, когда уже запустился код
//
//
// }


// let a = 10;
//
// function f(arg: any) {
//   let fScope = { // внутренний объект лексического окружения в которой ф-я была определена. То есть будет иметь доступ к a,c
//     // outerScope: globalScope, // ключ с ссылкой  на объект внешнего окружения, функция запустилась, она уже знает что у нее есть ссылка на объект внешнего ЛО
//     // arg: undefined,                // в момент запуска ф-ии ф-я знает обо всех аргументах, arg это имя переменной под капотом
//   }
//   var innerA = 50;
//   console.log(a);
//   a = innerA
// } // 2 проход код внутри скипается
// var c = 50;
//
// console.log(a)

// ?Var-let логи будут undef из-за особенностей всплытия
//* function f() {
//*   if () {
//*     let b = 0;
//*     console.log(a)
//*   } else {
//*     var a = 10
//*     console.log(b)
//*   }
//* }
//* f();
//* console.log(a)

//
// // ** Работа со скопом 32.32
//
// let globalScope = {[
//   //? Первый проход
//   outerScope: null,
//   f: 'function',
//   // c: undefined,
//   // ? Второй проход
//   a = 100,
//   c: 50, // Далее у нас есть вызов f() мы идем по ссылке f: func,
//   //!
//   funck: 'function',
// }
// let a = 10;
//
// function f(arg: any) { // Когда мы вызываем ф-ю, у нас создается новый объект ЛО fScope
//   let fScope = {
//     //? Первый проход
//     outerScope: globalScope,
//     // arg: undefined,
//     // innerA: undefined
//     //? После вызова
//     arg: 50,
//     innerA: 100
//     // ! Усложнение. В момент когда будет вызываться funck добавится новая переменная, но без значения, потому что ей надо посчитать чему будет равен let funck = f(c)
//     //!
//     funck:
//   }
//
//   var innerA = 100;
//   console.log(a); //? Эта А смотрится в скопе ф-ии и не находит а, смотрит если ссылка на глобалскоуп - есть.
//                   //? Лезет в объект globalScope и ищет а, находит и берет 10.
//   a = innerA //? Поиск а повторяется, и присваивает 100
//   console.log(arg) //? Сразу находится в локальной ОВ fScope 50 выводит
//
//   // ! Усложняем ситуацию добавляем еще
//
//   function f1() {
//     //! После обновления появляется f1Scope
//     let f1Scope = {
//       outerScope: fScope,
//     }
//     console.log(arg);
//     arg += 1000; //! После этого, арг находится в fScope по ссылке, и изменяется на 1000
//     return arg
//   }
//
//   return f1 //! Эта функция вернулась в фанк как результат, мы понимаем что нам вернулась ссылка на некую функцию из глобалскопа
// }
//
// var c = 50;
// //? f(c) Убрали для усложнения выше
// let funck = f(c)
// //! запускаем функцию funck. Вызывается f1(), ей создается новое ЛО,
// funck(); //! Добраться до ARG можно теперь только с помощью этой ф-ии
//
//
// console.log(a) //? а ищется в глобалскопе, и выводит 100


 // * Работа с таской 1
// Task 01
// Реализовать функцию sum которая суммирует 2 числа следующим образом sum(3)(6) === 9
// 1) Сначала вызывается ф-я sum с аргументом 3, ф-я возращает нам другую ф-ю и мы эту ф-ю вызываем на месте с аргументом 6
// Таким образом, возврат функции с аргументом 6, замкнута на аргументы ф-ии 3, вложенная ф-я 6 знает все что знала ф-я 3 (реализация замыкания)
// function sum(a: number) {
//   let sumScope = {
//   // @ts-ignore
//     outerScope: globalScope,
//     a: 3,
//   }
//
//   return function (b: number) {
//     let anonimScope = {
//       outerScope: sumScope,
//       b: 6
//     }
//     return a + b
//   }
// }
// console.log(sum(3)(6))


// * Работа с рекурсией
// sumTo(1) = 1
// sumTo(2) = 2 + 1 = 3
// sumTo(3) = 3 + 2 + 1 = 6
// sumTo(4) = 4 + 3 + 2 + 1 = 10
// ...
// sumTo(100) = 100 + 99 + ... + 2 + 1 = 5050

// function sumTo(arg: number) {
//   let result = 0
//   for (let i = 0; i <= arg; i++) {
//     result += i
//   }
//   return result
// }

// function sumTo(arg: number):number { // Рекурсивный подход
//   if (arg === 1) return arg;
//   return arg + sumTo(arg - 1);
// }






// Task 02
// Реализовать функцию makeCounter которая работает следующим образом:
// const counter = makeCounter();
// counter(); // 1
// counter(); // 2
// const counter2 = makeCounter();
// counter2(); // 1
// counter(); // 3

// Task 03
// Переписать функцию из Task 02 так, что бы она принимала число в качестве аргумента и это число было стартовым значением счетчика
// и возвращала следующий объект методов:
// increase: +1
// decrease: -1
// reset: установить счетчик в 0;
// set: установить счетчик в заданное значение;





// Task 04*
// Реализовать функцию superSum которая принимает число в качестве аргумента, которое указывает на количество слагаемых
// и что бы корректно работали следующие вызовы:
// 1) superSum(0) //0
// 2) superSum(3)(2)(5)(3) //10
// 3) superSum(3)(2)(5,3) //10
// 4) superSum(3)(2,5,3) //10
// 5) superSum(3)(2,5)(3) //10
// 6) superSum(3)(2,5)(3,9) //10

// P.S. типизируйте только аргументы, а при вызове функции используйте @ts-ignore





// Task 05
// решить все задачи по рекурсии которые даны в конце статьи https://learn.javascript.ru/recursion

// Task 06
// написать функцию, которая повторяет функционал метода flat массива на всю глубину.

// just a plug
export default () => {
};