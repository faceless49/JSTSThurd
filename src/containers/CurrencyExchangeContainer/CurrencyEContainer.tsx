import React from 'react';
import CurrencyExchange from '../../components/CurrencyExchange/CurrencyExchange';
import {CurrencyState, CurrencyType} from '../../redux/currencyReducer';
import {Dispatch} from 'redux';
import {
  changeActionAC,
  changeCurrencyFieldAC,
  changeCurrentCurrencyAC,
  CurrencyReducersTypes
} from '../../redux/actions';
import {connect, ConnectedProps, useDispatch, useSelector} from 'react-redux';
import {selectAllState} from '../../redux/selectors';

// * const CurrencyEContainer: React.FC<TProps> = props => {


const CurrencyEContainer: React.FC = () => {

  // === const {
  // === Потому что мы внизу в мапдиспатчтупропс прокидываем вместо последних трех AC
  //   currencies,
  //   currentCurrency,
  //   isBuying,
  //   amountOfBYN,
  //   amountOfCurrency,
  //   setCurrencyAmount,
  //   setAction,
  //   changeCurrency,
  // } = props;


  // const {
  //   currencies,
  //   currentCurrency,
  //   isBuying,
  //   amountOfBYN,
  //   amountOfCurrency,
  //   changeCurrencyFieldAC, // эти ф-ии пришли из библиотеки, не из импорта, внутри библиотека редакса проделала диспатч за нас
  //   changeActionAC,
  //   changeCurrentCurrencyAC
  // } = props;
  // === Это для классовых компонент для mapdispatchtoprops

  // === Пропсы для usedispatch without MDTP const {
  //   currencies,
  //   currentCurrency,
  //   isBuying,
  //   amountOfBYN,
  //   amountOfCurrency,
  // } = props;

  const dispatch = useDispatch<Dispatch<CurrencyReducersTypes>>() // Всегда из react-redux. Это используется уже в функц. компонентах.
  const {
    currencies,
    currentCurrency,
    isBuying,
    amountOfBYN,
    amountOfCurrency,
  } = useSelector(selectAllState);



  let currencyRate: number = 0;
  const currenciesName = currencies.map((currency: CurrencyType) => {
    if (currency.currencyName === currentCurrency) {
      currencyRate = isBuying ? currency.buyRate : currency.sellRate;
    }
    return currency.currencyName;
  });

  const changeCurrencyField = (e: React.ChangeEvent<HTMLInputElement>) => {
    let value = e.currentTarget.value;
    if (!isFinite(+value)) return;
    if (e.currentTarget.dataset.currency) {
      const trigger: string = e.currentTarget.dataset.currency;
      if (trigger === 'byn') {
        if (value === '') {
          // setCurrencyAmount(value, value);
          // changeCurrencyFieldAC(value, value); // Здесь это нам вернул библиотека редакс т.е это не наши импорты
          dispatch(changeCurrencyFieldAC(value, value));
        } else {
          // setCurrencyAmount(value, (+Number(value).toFixed(2) / currencyRate).toFixed(2));
          // changeCurrencyFieldAC(value, (+Number(value).toFixed(2) / currencyRate).toFixed(2)); // Здесь это нам вернул библиотека редакс т.е это не наши импорты
          dispatch(changeCurrencyFieldAC(value, (+Number(value).toFixed(2) / currencyRate).toFixed(2)));
        }
      } else {
        if (value === '') {
          // setCurrencyAmount(value, value);
          changeCurrencyFieldAC(value, value);
          dispatch(changeCurrencyFieldAC(value, value));
        } else {
          // setCurrencyAmount((+Number(value).toFixed(2) * currencyRate).toFixed(2), value);
          // changeCurrencyFieldAC((+Number(value).toFixed(2) * currencyRate).toFixed(2), value); // Здесь это нам вернул библиотека редакс т.е это не наши импорты
          dispatch(changeCurrencyFieldAC((+Number(value).toFixed(2) * currencyRate).toFixed(2), value));
        }
      }
    }
  };
  const changeAction = (e: React.MouseEvent<HTMLSpanElement>) => {
    // e.currentTarget.dataset.action === 'buy' ? setAction(true) : setAction(false);
    // e.currentTarget.dataset.action === 'buy' ? setAction(true) : setAction(false); // Здесь это нам вернул библиотека редакс т.е это не наши импорты
    e.currentTarget.dataset.action === 'buy' ? dispatch(changeActionAC(true)) : dispatch(changeActionAC(false));
  };

  const changeCurrentCurrency = (e: React.MouseEvent<HTMLLIElement>) => {
    // e.currentTarget.dataset.currency && changeCurrency(e.currentTarget.dataset.currency);
    // e.currentTarget.dataset.currency && changeCurrency(e.currentTarget.dataset.currency); // Здесь это нам вернул библиотека редакс т.е это не наши импорты
    e.currentTarget.dataset.currency && dispatch(changeCurrentCurrencyAC(e.currentTarget.dataset.currency));
  };

  return (
    <React.Fragment>
      <CurrencyExchange
        currenciesName={currenciesName}
        currentCurrency={currentCurrency}
        currencyRate={currencyRate}
        isBuying={isBuying}
        amountOfBYN={amountOfBYN}
        amountOfCurrency={amountOfCurrency}
        changeCurrencyField={changeCurrencyField}
        changeAction={changeAction}
        changeCurrentCurrency={changeCurrentCurrency}
      />
    </React.Fragment>
  );
};


// === const mapDispatchToProps = (dispatch: Dispatch<CurrencyReducersTypes>): any => {
//  ===  return {
//  ===    setCurrencyAmount(amountOfBYN: string, amountOfCurrency: string) {
//  ===      dispatch(changeCurrencyFieldAC(amountOfBYN, amountOfCurrency));
//  ===    },
//  ===    setAction(isBuying: boolean) {
//  ===      dispatch(changeActionAC(isBuying));
//  ===    },
//  ===    changeCurrency(currency: string) {
//  ===      dispatch(changeCurrentCurrencyAC(currency));
//  ===    },
//  ===  };
// ===
//  === const connector = connect(mapStateToProps, mapDispatchToProps);
// ** This code duplicate and harder then: можно писать и так, но больше кода, такой вариант предпочтительнее
// ** Соответственно мы должны заменить константы которые приходят в пропсах на AC

// const connector = connect(mapStateToProps, {
//   changeCurrencyFieldAC,
//   changeActionAC,
//   changeCurrentCurrencyAC,
// });
// === Это для классовых компонент


// ? Using UseSelector without mapstatetoprops and connector. This work latest. 1L. 1.34
//
// * const mapStateToProps = ({currency}: { currency: CurrencyState }): CurrencyState => { Здесь типизация пропсов при деструктуризации.
//   return {
//     currencies: currency.currencies,
//     currentCurrency: currency.currentCurrency,
//     isBuying: currency.isBuying,
//     amountOfBYN: currency.amountOfBYN,
//     amountOfCurrency: currency.amountOfCurrency,
//   };
// };
//
//
// const connector = connect(mapStateToProps, {
//   changeCurrencyFieldAC,
//   changeActionAC,
//   changeCurrentCurrencyAC,
// });
//
// type TProps = ConnectedProps<typeof connector>;

export default CurrencyEContainer;

